public class Position {
	
	public int x;
	public int y;
	
	public Position(int x, int y) {
		this.x = x; this.y = y;
	}
	
	public String toString() {
		return "(" + x + ", " + y + ")";
	}

	public boolean equals(Object o) {
		if(o == this) return true;
		if(o == null) return false;
		if(o.getClass() != this.getClass()) return false;
		Position that  = (Position) o;
		if(this.x == that.x && this.y == that. y) return true;
		return false;
	}

	public int hashCode() {
		int hash = 7;
		hash = 5 * (hash + this.x << 7);
		hash = 11 * hash + (this.y << 17);
		return hash;
	}

}
