public class State {
	public Orientation orientation;
	public Position position;
	public boolean turned_on;

	public State(Position position, Orientation orientation, boolean turned_on) {
		this.position = position;
		this.orientation = orientation;
		this.turned_on = turned_on;
	}

	public String toString() {
		return "State{position: " + position + ", orientation: " + orientation + ", on:" + turned_on + "}";
	}


	public boolean equals(Object o) {
		
		if(o == this) return true;
		if(o == null) return false;
		if(o.getClass() != this.getClass()) return false;
		State that  = (State) o;
		if(this.turned_on == that.turned_on && this.orientation.equals(that.orientation) && this.position.equals(that.position)) return true;
		return false;
	}

	public int hashCode() {
		int hash = 1933;
		hash = 3 * hash + (orientation.ordinal() << 3);
		hash = 11 * hash + position.hashCode() >> 4 << 11;
		hash = 7 * hash + ((turned_on ? 1 : 0) << 2);

		return hash;
	}


}